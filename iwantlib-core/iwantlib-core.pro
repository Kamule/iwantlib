TARGET = iwantlib-core
TEMPLATE = lib
CONFIG += staticlib

HEADERS += \
    include/iwantlib/tools/updater.hpp \
   include/iwantlib/tools/worker.hpp

SOURCES += \
    src/tools/updater.cpp \
    src/tools/worker.cpp

INCLUDEPATH += $$PWD/include/
